# Changelog

## [24.1.0] - 30-01-2024

* Add dependencies on robot_state_publisher
* Upgrade cmake minimum version

## [20.10.0] - 26-10-2020

* Adapted configuration for new release

## [3.2.0] - 06-11-2019

* Applied cola2 lib refactor changes

## [3.1.0] - 25-02-2019

* First release
